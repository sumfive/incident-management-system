﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using EAGetMail;
using Newtonsoft.Json;
using System.Data;
using System.Data.Common;
using ims.DTO;
using System.Net.Mail;
using System.Net;
using System.Globalization;
using System.Security.Cryptography.X509Certificates;
using System.Net.Sockets;
using System.Net.Security;
using System.Text.RegularExpressions;

namespace ims
{
    class Program
    {
        static void Main(string[] args)
        {
            Program main = new Program();
            List<ProjectEmailData> ProjectsDataList = main.GetProjectEmails();
            foreach (var data in ProjectsDataList)
            {
                main.getMailForProject(data);
            }
        }

        public List<ProjectEmailData> GetProjectEmails()
        {
            List<ProjectEmailData> ProjectsDataList = new List<ProjectEmailData>();
            using (DbConnection conn = DAOSettings.Factory.CreateConnection())
            {
                conn.ConnectionString = DAOSettings.ConnectionString;
                conn.Open();
                DbTransaction transaction = conn.BeginTransaction();
                using (DbCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "sp_get_project_emails_data";
                    DbParameter p_refcursor = cmd.CreateParameter();
                    p_refcursor.ParameterName = "ref";
                    p_refcursor.Direction = ParameterDirection.Output;
                    p_refcursor.DbType = DbType.Object;
                    cmd.Parameters.Add(p_refcursor);

                    cmd.ExecuteNonQuery();
                    var myReader = cmd.Parameters["ref"].Value;

                    cmd.CommandText = "fetch all from \"" + myReader + "\"";
                    cmd.CommandType = CommandType.Text;
                    DbDataReader dataReader = cmd.ExecuteReader();

                    while (dataReader.Read())
                    {
                        ProjectEmailData objProjectEmailData = new ProjectEmailData();
                        objProjectEmailData.ProjectId = Convert.ToInt32(dataReader["project_id"]);
                        objProjectEmailData.OrganizationId = Convert.ToInt32(dataReader["organization_id"]);
                        objProjectEmailData.EmailServerType = Convert.ToString(dataReader["mail_server_type"]);
                        objProjectEmailData.EmailServer = Convert.ToString(dataReader["mail_server"]);
                        objProjectEmailData.EmailId = Convert.ToString(dataReader["mail_id"]);
                        objProjectEmailData.Password = Convert.ToString(dataReader["password"]);
                        objProjectEmailData.DepartmentId = Convert.ToInt32(dataReader["department_id"]);
                        objProjectEmailData.AssignedTo = Convert.ToInt32(dataReader["assigned_to"]);
                        objProjectEmailData.StatusId = Convert.ToInt32(dataReader["status_id"]);
                        if( dataReader["department_id"] != DBNull.Value){
                            objProjectEmailData.DepartmentId = Convert.ToInt32(dataReader["department_id"]);
                        }
                        ProjectsDataList.Add(objProjectEmailData);
                    }
                    dataReader.Close();
                }
                transaction.Commit();
                conn.Close();
            }
            return ProjectsDataList;
        }
        public void getMailForProject(ProjectEmailData objProjectEmailData)
        {
            // Create a folder named "inbox" under current directory
            // to save the email retrieved.
            var issueId =0;

            MailServer oServer = new MailServer(objProjectEmailData.EmailServer,
                        objProjectEmailData.EmailId, objProjectEmailData.Password, objProjectEmailData.EmailServerType =="POP3" ? ServerProtocol.Pop3 : ServerProtocol.Imap4);

            MailClient oClient = new MailClient("TryIt");
             if (objProjectEmailData.EmailServerType == "POP3")
            {
                oServer.SSLConnection = true;
                oServer.Port = 995;
            }else{
                // oServer.Port = 143;
                oServer.SSLConnection = true;
                oServer.Port = 993;
            }
            try
            {
                oClient.Connect(oServer);
                MailInfo[] infos = oClient.GetMailInfos();
                for (int i = 0; i < infos.Length; i++)
                {
                    MailInfo info = infos[i];
                    Console.WriteLine("Index: {0}; Size: {1}; UIDL: {2}",
                        info.Index, info.Size, info.UIDL);

                    // Receive email from POP3 server
                    Mail oMail = oClient.GetMail(info);

                    Console.WriteLine("From: {0}", oMail.From.ToString());
                    Console.WriteLine("Subject: {0}\r\n", oMail.Subject);
                    System.Text.RegularExpressions.Regex rx = new System.Text.RegularExpressions.Regex("<[^>]*>");
                    var from = oMail.From.ToString();
                    var Subject = oMail.Subject;
                    var body = rx.Replace(oMail.HtmlBody, "");
                    Program main = new Program();
                     if (Subject.ToLower().IndexOf('[') != -1 && Subject.ToLower().IndexOf(']') != -1) {                         
                        int startindex = Subject.IndexOf('[');
                        int Endindex = Subject.IndexOf(']');
                        string issueID = Subject.Substring(startindex + 1, Endindex - startindex - 1);
                        main.updateTask(body, Convert.ToInt32(issueID));                       
                        main.GetUserUpdateEmailsandIssueDetails(objProjectEmailData, from, Subject, body, issueId, from);
                    }else{
                        string FromMail = main.emas(from);
                        issueId = main.addTask(objProjectEmailData, from, Subject, body ,FromMail);
                        main.GetUserNewEmailsandIssueDetails(objProjectEmailData, from, Subject, body, issueId, from);
                    }
                    
                    
                    // oClient.Delete(info);
                }

                // Quit and purge emails marked as deleted from POP3 server.
                oClient.Quit();
            }
            catch (Exception ep)
            {
                Console.WriteLine(ep.Message);
            }

        }
        public string emas(string text)
        {
            string email = "";
            const string MatchEmailPattern =
           @"(([\w-]+\.)+[\w-]+|([a-zA-Z]{1}|[\w-]{2,}))@"
           + @"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\."
             + @"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
           + @"([a-zA-Z]+[\w-]+\.)+[a-zA-Z]{2,4})";
            Regex rx = new Regex(MatchEmailPattern,  RegexOptions.Compiled | RegexOptions.IgnoreCase);
            // Find matches.
            MatchCollection matches = rx.Matches(text);
            // Report the number of matches found.
            int noOfMatches = matches.Count;
            // Report on each match.
            foreach (Match match in matches)
            {
                email = match.Value.ToString();
            }
            return email;
        }
         public bool checkInternalOrExternalUser(string From){
             bool isInternalUser = false;
                         using (DbConnection conn = DAOSettings.Factory.CreateConnection())
            {
                conn.ConnectionString = DAOSettings.ConnectionString;
                conn.Open();
                DbTransaction transaction = conn.BeginTransaction();
                using (DbCommand cmd = conn.CreateCommand())
                {

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "sp_check_user_exixt";

                    DbParameter p_user_mail = cmd.CreateParameter();
                    p_user_mail.ParameterName = "p_user_mail";
                    p_user_mail.Value = From;
                    cmd.Parameters.Add(p_user_mail);

                    var Id = cmd.ExecuteScalar();
                    isInternalUser = Convert.ToBoolean(Id);
                    transaction.Commit();
                }
                conn.Close();
            }

             return isInternalUser;

         }
        public int addTask(ProjectEmailData objProjectEmailData, string from, string Subject, string body, string From)
        {
            int issue_id = 0;
            using (DbConnection conn = DAOSettings.Factory.CreateConnection())
            {
                conn.ConnectionString = DAOSettings.ConnectionString;
                conn.Open();
                DbTransaction transaction = conn.BeginTransaction();
                using (DbCommand cmd = conn.CreateCommand())
                {

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "sp_add_issue_from_mail";

                    DbParameter p_organization_id = cmd.CreateParameter();
                    p_organization_id.ParameterName = "p_organization_id";
                    p_organization_id.Value = objProjectEmailData.OrganizationId;
                    cmd.Parameters.Add(p_organization_id);

                    DbParameter p_department_id = cmd.CreateParameter();
                    p_department_id.ParameterName = "p_department_id";
                    if (objProjectEmailData.DepartmentId != null)
                    {
                        p_department_id.Value = objProjectEmailData.DepartmentId;
                    }
                    else
                    {
                        p_department_id.Value = DBNull.Value;
                    }
                    cmd.Parameters.Add(p_department_id);

                    DbParameter p_notes = cmd.CreateParameter();
                    p_notes.ParameterName = "p_notes";
                    p_notes.Value = body;
                    cmd.Parameters.Add(p_notes);

                    DbParameter p_status_id = cmd.CreateParameter();
                    p_status_id.ParameterName = "p_status_id";
                    if (objProjectEmailData.StatusId != null)
                    {
                        p_status_id.Value = objProjectEmailData.StatusId;
                    }
                    else
                    {
                        p_status_id.Value = DBNull.Value;
                    }
                    cmd.Parameters.Add(p_status_id);



                    DbParameter p_title = cmd.CreateParameter();
                    p_title.ParameterName = "p_title";
                    p_title.Value = Subject;
                    cmd.Parameters.Add(p_title);

                    DbParameter p_start_date = cmd.CreateParameter();
                    p_start_date.ParameterName = "p_start_date";
                    p_start_date.Value = DateTime.Now;
                    cmd.Parameters.Add(p_start_date);

                    DbParameter p_end_date = cmd.CreateParameter();
                    p_end_date.ParameterName = "p_end_date";
                    p_end_date.Value = DateTime.Now; ;
                    cmd.Parameters.Add(p_end_date);

                    List<int> users = new List<int>();
                    users.Add(objProjectEmailData.AssignedTo);

                    DbParameter p_user_ids = cmd.CreateParameter();
                    p_user_ids.ParameterName = "p_user_ids";
                    p_user_ids.Value = users;
                    cmd.Parameters.Add(p_user_ids);

                    DbParameter p_group_ids = cmd.CreateParameter();
                    p_group_ids.ParameterName = "p_group_ids";
                    p_group_ids.Value = DBNull.Value;
                    cmd.Parameters.Add(p_group_ids);


                    DbParameter p_user_id = cmd.CreateParameter();
                    p_user_id.ParameterName = "p_user_id";
                    p_user_id.Value = objProjectEmailData.AssignedTo;
                    cmd.Parameters.Add(p_user_id);

                    DbParameter p_project_id = cmd.CreateParameter();
                    p_project_id.ParameterName = "p_project_id";
                    p_project_id.Value = objProjectEmailData.ProjectId;
                    cmd.Parameters.Add(p_project_id);

                    DbParameter p_severity_id = cmd.CreateParameter();
                    p_severity_id.ParameterName = "p_severity_id";
                    p_severity_id.Value = DBNull.Value;
                    cmd.Parameters.Add(p_severity_id);

                    DbParameter p_priority_id = cmd.CreateParameter();
                    p_priority_id.ParameterName = "p_priority_id";
                    p_priority_id.Value = DBNull.Value;
                    cmd.Parameters.Add(p_priority_id);

                    DbParameter p_classification_id = cmd.CreateParameter();
                    p_classification_id.ParameterName = "p_classification_id";
                    p_classification_id.Value = DBNull.Value;
                    cmd.Parameters.Add(p_classification_id);

                    DbParameter p_email = cmd.CreateParameter();
                    p_email.ParameterName = "p_email";
                    p_email.Value = From;
                    cmd.Parameters.Add(p_email);

                    var Id = cmd.ExecuteScalar();
                    issue_id = Convert.ToInt32(Id);
                    transaction.Commit();
                }
                conn.Close();
            }
            return issue_id;
        }
        public void GetUserNewEmailsandIssueDetails(ProjectEmailData objProjectEmailData, string from, string Subject, string body, int issueId, string From)
        {
            // EmailTemplatesDAO objMail = new EmailTemplatesDAO();
            IssuesEmail objIssuesEmail = new IssuesEmail();
            objIssuesEmail.Title = Subject;
            Program main = new Program();           
            objIssuesEmail.AssignTo = From;
            string subject = objIssuesEmail.Title;
            EmailProperties objEmailProperties = new EmailProperties();
            objEmailProperties.ToEmails = objIssuesEmail.AssignTo;
            objEmailProperties.Subject = "[" + issueId + "] " + subject;            
            string htmlString = @"<html>
                      <body>
                      <p>Hi,</p>
                      <p>Your issue {{TITLE}} has been received and has been assigned to issue number [{{IssueId}}].  Pleae keep this Issue number for your records and include it in the subject line (including brackets) of all future emails relating to this isuue:.</p>
                      <p>Thanks & Regards,<br>IMS Admin</br></p>
                      </body>
                      </html>";
            //objEmailProperties.Body = objEmailTemplate.BodyTemplate;
            htmlString = htmlString.Replace("{{TITLE}}", objIssuesEmail.Title);
            htmlString = htmlString.Replace("{{IssueId}}", issueId.ToString());
            objEmailProperties.Body = htmlString;
            objEmailProperties.FromMailId = objProjectEmailData.EmailId;
            objEmailProperties.FromPassword = objProjectEmailData.Password;

            main.SendMailForProjectBoardCreationsUpdations(objEmailProperties);
        }

        public void GetUserUpdateEmailsandIssueDetails(ProjectEmailData objProjectEmailData, string from, string Subject, string body, int issueId, string From)
        {
            // EmailTemplatesDAO objMail = new EmailTemplatesDAO();
            IssuesEmail objIssuesEmail = new IssuesEmail();
            objIssuesEmail.Title = Subject;
            Program main = new Program();           
            objIssuesEmail.AssignTo = From;
            string subject = objIssuesEmail.Title;
            EmailProperties objEmailProperties = new EmailProperties();
            objEmailProperties.ToEmails = objIssuesEmail.AssignTo;
            objEmailProperties.Subject = "[" + issueId + "] " + subject;            
            string htmlString = @"<html>
                      <body>
                      <p>Hi,</p>
                      <p>Your issue {{TITLE}} has been updated and has been assigned to issue number [{{IssueId}}].  Pleae keep this Issue number for your records and include it in the subject line (including brackets) of all future emails relating to this isuue:.</p>
                      <p>Thanks & Regards,<br>IMS Admin</br></p>
                      </body>
                      </html>";
            //objEmailProperties.Body = objEmailTemplate.BodyTemplate;
            htmlString = htmlString.Replace("{{TITLE}}", objIssuesEmail.Title);
            htmlString = htmlString.Replace("{{IssueId}}", issueId.ToString());
            objEmailProperties.Body = htmlString;
            objEmailProperties.FromMailId = objProjectEmailData.EmailId;
            objEmailProperties.FromPassword = objProjectEmailData.Password;

            main.ReplayMailForIssue(objEmailProperties);
        }
        public bool SendMailForProjectBoardCreationsUpdations(EmailProperties objEmailProperties)
        {
            string smtpAddress = DAOSettings.SmtpAddress;
            int portNumber = Convert.ToInt32(DAOSettings.PortNumber);
            bool enableSSL = true;
            string emailFrom = objEmailProperties.FromMailId;
            string password = objEmailProperties.FromPassword;
            try
            {
                using (MailMessage mail = new MailMessage())
                {
                    mail.From = new System.Net.Mail.MailAddress(emailFrom);
                    mail.To.Add(new System.Net.Mail.MailAddress(objEmailProperties.ToEmails));
                    mail.Subject = objEmailProperties.Subject;
                    mail.Body = objEmailProperties.Body;
                    mail.BodyEncoding = System.Text.Encoding.GetEncoding("utf-8");
                    mail.SubjectEncoding = System.Text.Encoding.Default;
                    mail.IsBodyHtml = true;

                    using (SmtpClient smtp = new SmtpClient(smtpAddress, portNumber))
                    {
                        ServicePointManager.ServerCertificateValidationCallback =
                        delegate (object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
                        { return true; };
                        smtp.Credentials = new NetworkCredential(emailFrom, password);
                        smtp.EnableSsl = enableSSL;
                        smtp.Send(mail);
                    }
                }
            }
            catch (Exception e)
            {
                e.ToString();
            }
            return true;
        }

        public bool ReplayMailForIssue(EmailProperties objEmailProperties)
        {
            string smtpAddress = DAOSettings.SmtpAddress;
            int portNumber = Convert.ToInt32(DAOSettings.PortNumber);
            bool enableSSL = true;
            string emailFrom = objEmailProperties.FromMailId;
            string password = objEmailProperties.FromPassword;
            try
            {
                using (MailMessage mail = new MailMessage())
                {
                    mail.From = new System.Net.Mail.MailAddress(emailFrom);
                    mail.To.Add(new System.Net.Mail.MailAddress(objEmailProperties.ToEmails));
                    mail.Subject = objEmailProperties.Subject;
                    mail.Body = objEmailProperties.Body;
                    mail.BodyEncoding = System.Text.Encoding.GetEncoding("utf-8");
                    mail.SubjectEncoding = System.Text.Encoding.Default;
                    mail.IsBodyHtml = true;
                    using (SmtpClient smtp = new SmtpClient(smtpAddress, portNumber))
                    {
                        ServicePointManager.ServerCertificateValidationCallback =
                        delegate (object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
                        { return true; };
                        smtp.Credentials = new NetworkCredential(emailFrom, password);
                        smtp.EnableSsl = enableSSL;
                        smtp.Send(mail);
                    }
                }
            }
            catch (Exception e)
            {
                e.ToString();
            }
            return true;
        }
      
         public void updateTask(string body, int issueId)
        {
            int issue_id = 0;
            using (DbConnection conn = DAOSettings.Factory.CreateConnection())
            {
                conn.ConnectionString = DAOSettings.ConnectionString;
                conn.Open();
                DbTransaction transaction = conn.BeginTransaction();
                using (DbCommand cmd = conn.CreateCommand())
                {

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "sp_update_issue_from_mail";

                    DbParameter p_issue_id = cmd.CreateParameter();
                    p_issue_id.ParameterName = "p_issue_id";
                    p_issue_id.Value = issueId;
                    cmd.Parameters.Add(p_issue_id);

                    DbParameter p_notes = cmd.CreateParameter();
                    p_notes.ParameterName = "p_notes";
                    p_notes.Value = body;
                    cmd.Parameters.Add(p_notes);

                    cmd.ExecuteScalar();
                    transaction.Commit();
                }
                conn.Close();
            }
        }
       
    }
}
