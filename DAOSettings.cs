using System;
using System.Data;
using System.Data.Common;
using Microsoft.Extensions.Configuration;
using Npgsql;

namespace ims
{
    public static class DAOSettings
    {
        public static DbProviderFactory Factory = Npgsql.NpgsqlFactory.Instance;

        private static IConfigurationRoot Configuration = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build();
        public static string ConnectionString = Configuration["ApplicationSettings:ConnectionString"];

        public static string SmtpAddress = Configuration["MailSettings:SmtpAddress"];
        public static string PortNumber = Configuration["MailSettings:PortNumber"];
        public static string UserName = Configuration["MailSettings:UserName"];
        public static string EmailFrom = Configuration["MailSettings:EmailFrom"];
        public static string Password = Configuration["MailSettings:Password"];
        public static string WebSiteUrl = Configuration["MailSettings:WebSiteUrl"];
    }
}
