	
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ims.DTO
{
    public class AddIssues
    {
        public int? OrganizationId {get; set;}
        public int? DepartmentId {get; set;}
        public string Notes {get; set;}

        public int? StatusId {get; set;}

        public string Title { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public List<int> UserIds { get; set;}
        public List<int> GroupIds { get; set; }
         public List<string> UserNames {get; set;}
         public List<string> GroupNames {get;set;}
         public int ProjectId {get; set;}

         public int? SeverityId{ get; set;}

         public int? PriorityId {get; set;}

         public int? ClassificationId{get;set;}
     
    }
}