	
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ims.DTO
{
    public class IssuesEmail
    {
        public string Title { get; set; }
        public string AssignTo{get;set;}
        public string CreatedBy {get; set;}

        public string ModifiedBy {get;set;}
     
    }
}