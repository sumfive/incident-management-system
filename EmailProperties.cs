	
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ims.DTO
{
    public class EmailProperties
    {
        public string ToEmails { get; set; }
        public string Subject{get;set;}
        public string Body {get; set;}
        public string FromMailId {get; set; }
        public string FromPassword {get;set;}
     
    }
}