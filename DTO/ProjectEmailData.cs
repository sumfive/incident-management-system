using System;
using System.Collections.Generic;

namespace ims.DTO
{
    public class ProjectEmailData 
    {
        public int ProjectId { get; set; }
        public int OrganizationId { get; set; }
        public string EmailServerType { get; set; }
        public string EmailServer { get; set; }
        public string EmailId {get; set; }
        public string Password {get; set; }    
        public int? DepartmentId {get; set;}
        public int AssignedTo {get;set;}
        public int? StatusId {get;set;} 
    }
}
